package utp.edu.appfarmacia;

import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.Arrays;
import java.util.List;

import utp.edu.appfarmacia.clases.Producto;
import utp.edu.appfarmacia.clases.UserGlobal;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    Producto producto;
    Toolbar _toolbarMap;
    double latitudE, longitudE;
    UserGlobal userGlobal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        userGlobal = new UserGlobal();
        producto = (Producto) getIntent().getSerializableExtra("dato");

        _toolbarMap = findViewById(R.id.toolbarMap);
        setSupportActionBar(_toolbarMap);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle(producto.getEstablecimiento());

        latitudE = producto.getLatitud();
        longitudE = producto.getLongitud();

        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Ubicacion establecimiento
        LatLng establecimiento = new LatLng(latitudE, longitudE);
        mMap.addMarker(new MarkerOptions()
                .position(establecimiento)
                .title(producto.getEstablecimiento())
                .snippet(producto.getDireccion() + ".\n " + producto.getDireccion())
        ).showInfoWindow();

        if(userGlobal.getLatitud()!= 0){
            //Ubicacion local
            LatLng local = new LatLng(userGlobal.getLatitud(), userGlobal.getLongitud());
            mMap.addMarker(new MarkerOptions()
                    .position(local)
                    .title("Tu Ubicación Actual")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_location))).showInfoWindow();

            List<PatternItem> patternd = Arrays.asList(
                    new Dash(20), new Gap(20)); //Dot()

            Polyline polyline = mMap.addPolyline(new PolylineOptions()
                    .add(establecimiento, local)
                    .width(25)
                    .color(Color.RED)
                    .geodesic(true).pattern(patternd));

        }


        // polyline.setEndCap( new CustomCap(BitmapDescriptorFactory.fromResource(R.drawable.icon_arrow), 40));
        mMap.getUiSettings().setZoomControlsEnabled(true);
        //LatLng lima = new LatLng(-12.022573904993147, -77.04134589535569);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitudE, longitudE), 10));

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}