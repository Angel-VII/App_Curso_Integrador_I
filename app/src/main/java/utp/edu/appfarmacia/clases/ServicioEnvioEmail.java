package utp.edu.appfarmacia.clases;

import android.content.Context;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.activation.CommandMap;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.MailcapCommandMap;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

public class ServicioEnvioEmail {
    private static ServicioEnvioEmail instance = null;
    private static Context ctx;
    UserGlobal userGlobal = new UserGlobal();
    //CORREO DEL REMITENTE
    final String username = "digif@email.com";

    ByteArrayOutputStream outputStream = null;

    Properties prop;
    Session session;
    public static final ExecutorService emailExecutor = Executors.newSingleThreadExecutor();

    //INICIAR SESION DE CORREO
    private ServicioEnvioEmail(Context context) {
        ctx = context;

        MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
        mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
        mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
        mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
        mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
        mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
        CommandMap.setDefaultCommandMap(mc);
        //HOST DE INICIO DE SESION DE REMITENTE
        prop = new Properties();
        prop.put("mail.smtp.host", "smtp.mail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.ssl.trust", "smtp.mail.com");

        session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        //contraseña del remitente (mantener dato privado)
                        return new PasswordAuthentication(username, "digi-farma");
                    }
                });
    }

    public static synchronized ServicioEnvioEmail getInstance(Context context) {
        if (instance == null) {
            instance = new ServicioEnvioEmail(context);
        }
        return instance;
    }

    //Enviando REPORTE ADJUNTO
    public void SendEmail() {
        try {
            //CORREO DE DESTINO
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(userGlobal.getCorreo().trim())
            );
            message.setSubject("Reporte DigiFarma");
            message.setText("Hola " + UserGlobal.getNombreUsuario() + ",\nSe ha confirmado su pedido. Revise su comprobante adjunto\n\nDigiFarma.");

            Multipart multipart = new MimeMultipart();


            //PDF contentenido para output stream
            outputStream = new ByteArrayOutputStream();
            writePdf(outputStream);
            byte[] bytes = outputStream.toByteArray();

            //constructor pdf body part
            DataSource dataSource = new ByteArrayDataSource(bytes, "application/pdf");
            MimeBodyPart pdfBodyPart = new MimeBodyPart();
            pdfBodyPart.setDataHandler(new DataHandler(dataSource));
            pdfBodyPart.setFileName("reporte.pdf");
            pdfBodyPart.setHeader("Content-ID", "<aplication>");
            multipart.addBodyPart(pdfBodyPart);

            message.setContent(multipart);

            Transport.send(message);

        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Escribir PDF
    public void writePdf(OutputStream outputStream) throws Exception {


        Document document = new Document();
        PdfWriter.getInstance(document, outputStream);

        document.open();

        document.addTitle("Reporte DigiFarma");
        document.addSubject("Reporte de compra");
        document.addKeywords("iText, email");
        document.addAuthor("DigiFarma");
        document.addCreator("DigiFarma");


        Font font1 = FontFactory.getFont(BaseFont.HELVETICA_BOLD);
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("EEEE dd/MM/yyyy HH:mm");
        String fecha = df.format(c);
        //Font font2= new Font(Font.FontFamily.HELVETICA,14,Font.BOLD);

        Paragraph paragraph = new Paragraph();
        paragraph.add(new Chunk("Reporte", FontFactory.getFont(BaseFont.HELVETICA, 22, Font.BOLD)));
        paragraph.add(new Chunk("\nDigiFarma"));
        paragraph.add(new Chunk("\n" + fecha));
        paragraph.add(new Chunk("\n\n" + userGlobal.getNombreUsuario()));
        paragraph.add(new Chunk("\n" + "Id: " + userGlobal.getDni()));
        paragraph.add(new Chunk("\n" + "Tipo de Pago: " + (userGlobal.getTipoPago() == 1 ? "Tarjeta" : "Efectivo")));

        paragraph.setSpacingAfter(20);
        document.add(paragraph);


        PdfPTable tablista = new PdfPTable(5);
        tablista.setWidths(new float[]{0.7f, 3, 3, 1.5f, 1.5f});

        tablista.addCell(new Phrase("N°", font1));
        tablista.addCell(new Phrase("Item", font1));
        tablista.addCell(new Phrase("Establecimiento", font1));
        tablista.addCell(new Phrase("Cantidad", font1));
        tablista.addCell(new Phrase("Monto", font1));

        int i = 0;
        for (Pedido pedido : userGlobal.getListaPedidos()) {
            i++;
            tablista.addCell(String.valueOf(i));
            tablista.addCell(pedido.getProducto() + " (" + pedido.getIdProducto() + ")");
            tablista.addCell(pedido.getEstablecimiento());
            tablista.addCell(String.valueOf(pedido.getCantidad()));
            tablista.addCell(formatoPrecio(pedido.getPrecioSubTotal()));
        }

        PdfPCell cellTotal = new PdfPCell(new Phrase("Total: " + formatoPrecio(userGlobal.getPrecioTotal()), font1));
        cellTotal.setColspan(5);
        cellTotal.setHorizontalAlignment(Element.ALIGN_RIGHT);
        tablista.addCell(cellTotal);

        document.add(tablista);
        document.close();
    }

    public String formatoPrecio(double precio) {
        DecimalFormat formato = new DecimalFormat("#0.00");
        return "S/. " + formato.format(precio);
    }

}
