package utp.edu.appfarmacia.clases;

public class Pedido extends Producto{

    private int cantidad;

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecioSubTotal() {
        return this.cantidad * this.getPrecioUni();
    }

}
