package utp.edu.appfarmacia.clases;

import android.Manifest;
import android.app.Application;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import java.util.List;

public class UserGlobal extends Application {
    static String nombreUsuario;
    static String dni;
    static String correo;
    static int tipoPago;

    static double latitud;
    static double longitud;

    static List<Pedido> listaPedidos;

    private LocationManager locationManager;

    public static String getNombreUsuario() {
        return nombreUsuario;
    }

    public static void setNombreUsuario(String nombreUsuario) {
        UserGlobal.nombreUsuario = nombreUsuario;
    }

    public static String getDni() {
        return dni;
    }

    public static void setDni(String dni) {
        UserGlobal.dni = dni;
    }

    public static int getTipoPago() {
        return tipoPago;
    }

    public static void setTipoPago(int tipoPago) {
        UserGlobal.tipoPago = tipoPago;
    }

    public static String getCorreo() {
        return correo;
    }

    public static void setCorreo(String correo) {
        UserGlobal.correo = correo;
    }

    public static List<Pedido> getListaPedidos() {
        return listaPedidos;
    }

    public static void setListaPedidos(List<Pedido> listaPedidos) {
        UserGlobal.listaPedidos = listaPedidos;
    }


    public static double getPrecioTotal() {
        double precioT = 0;
        if (listaPedidos != null) {
            for (int i = 0; i < listaPedidos.size(); i++) {
                precioT += listaPedidos.get(i).getPrecioSubTotal();
            }
        }
        return precioT;
    }


    public static double getLatitud() {
        return latitud;
    }

    public static double getLongitud() {
        return longitud;
    }

    private static void setLatitud(double latitud) {
        UserGlobal.latitud = latitud;
    }

    private static void setLongitud(double longitud) {
        UserGlobal.longitud = longitud;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        //---permisos:
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Por favor, encienda su GPS para realizar las operaciones.", Toast.LENGTH_LONG).show();
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location location) {
                //---Ubicaciones en el mapa:
                setLatitud(location.getLatitude());
                setLongitud(location.getLongitude());
            }

            @Override
            public void onProviderDisabled(@NonNull String provider) {

                Toast.makeText(getApplicationContext(), "Por favor, encienda su GPS para realizar las operaciones.", Toast.LENGTH_LONG).show();
            }
        });
    }
}

