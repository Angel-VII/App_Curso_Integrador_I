package utp.edu.appfarmacia.clases;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class ConexionBase {

    String url = "http://10.0.2.2/farmaControlador/controlador.php";//url servidor local emulador
    
    Context context;
    String idPedidos;

    public String getUrl() {
        return url;
    }
    public ConexionBase(){}

    public ConexionBase(Context context) {
        this.context = context;
    }
    public interface VolleyResponseListener{
        void onResponse(String idPedidos);
        void onError(String error);
    }

    public void getIdPedidos(String enlace , VolleyResponseListener volleyResponseListener  ) {
        RequestQueue cola = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, enlace
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                idPedidos = response;
                //Toast.makeText(context, response, Toast.LENGTH_SHORT).show();
                volleyResponseListener.onResponse(idPedidos);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.w("ERROR", error.getMessage());
                volleyResponseListener.onError("Error");
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        cola.add(stringRequest);

    }

}
