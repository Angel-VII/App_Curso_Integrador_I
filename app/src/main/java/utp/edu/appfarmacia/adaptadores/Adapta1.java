package utp.edu.appfarmacia.adaptadores;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.util.List;

import utp.edu.appfarmacia.ProductoActivity;
import utp.edu.appfarmacia.R;
import utp.edu.appfarmacia.clases.Producto;


public class Adapta1 extends RecyclerView.Adapter<Adapta1.MyHolder> {
    List<Producto> lista;
    Context contexto;

    public Adapta1(List<Producto> lista, Context contexto) {
        this.lista = lista;
        this.contexto = contexto;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vista1, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        final Producto productoObj = lista.get(position);

        holder._txtProducto.setText(productoObj.getProducto());
        holder._txtStock.setText("Stock: " + productoObj.getStock() + " Unid.");
        holder._txtPrecioUni.setText(formatoPrecio(productoObj.getPrecioUni()));
        holder._layoutP.setOnClickListener(new View.OnClickListener() {//Accion al hacer click en el layout de una fila
            @Override

            public void onClick(View view) {

                Intent intent = new Intent(contexto, ProductoActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("productObjIntent", productoObj);
                contexto.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        TextView _txtProducto, _txtStock, _txtPrecioUni;
        ConstraintLayout _layoutP;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            _layoutP = itemView.findViewById(R.id.layoutProducto);
            _txtProducto = itemView.findViewById(R.id.txtProductoPd);
            _txtStock = itemView.findViewById(R.id.txtCantidadPd);
            _txtPrecioUni = itemView.findViewById(R.id.txtPrecioPd);

        }
    }
    public String formatoPrecio(double precio) {
        DecimalFormat formato = new DecimalFormat("#0.00");
        return "S/. " + formato.format(precio);
    }
}
