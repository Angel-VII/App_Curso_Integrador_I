package utp.edu.appfarmacia.adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.util.List;

import utp.edu.appfarmacia.R;
import utp.edu.appfarmacia.clases.Pedido;


public class Adapta2 extends RecyclerView.Adapter<Adapta2.MyHolder> {
    List<Pedido> lista;
    Context contexto;

    public Adapta2(List<Pedido> lista, Context contexto) {
        this.lista = lista;
        this.contexto = contexto;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vista1, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        final Pedido pedido = lista.get(position);

        holder._txtProducto.setText(pedido.getProducto());
        holder._txtCantidad.setText("Cantidad: " + pedido.getCantidad() + " Unid.");
        holder._txtPrecioxCantidad.setText(formatoPrecio(pedido.getPrecioSubTotal()));

    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        TextView _txtProducto, _txtCantidad, _txtPrecioxCantidad;


        public MyHolder(@NonNull View itemView) {
            super(itemView);

            _txtProducto = itemView.findViewById(R.id.txtProductoPd);
            _txtCantidad = itemView.findViewById(R.id.txtCantidadPd);
            _txtPrecioxCantidad = itemView.findViewById(R.id.txtPrecioPd);

        }
    }
    public String formatoPrecio(double precio) {
        DecimalFormat formato = new DecimalFormat("#0.00");
        return "S/. " + formato.format(precio);
    }
}
