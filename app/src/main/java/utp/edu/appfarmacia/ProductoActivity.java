package utp.edu.appfarmacia;

import android.Manifest;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import utp.edu.appfarmacia.clases.Pedido;
import utp.edu.appfarmacia.clases.Producto;
import utp.edu.appfarmacia.clases.UserGlobal;


public class ProductoActivity extends AppCompatActivity {

    TextView _txtPrecioUni, _txtCategoria, _txtLab, _txtDescripcion;
    TextView _txtNombreEstab, _txtDireccion, _txtUbicacion;
    Toolbar _toolBar;
    FloatingActionButton _fabAgregaPedido;
    Producto producto;
    Pedido pedido;
    UserGlobal global;
    List<Pedido> lista;

    int cantidad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);
        _fabAgregaPedido = findViewById(R.id.fabAgregapedido);
        _toolBar = findViewById(R.id.toolbarPr);
        _txtPrecioUni = findViewById(R.id.txtPrecioUniPr);
        _txtCategoria = findViewById(R.id.txtCategoriaPr);
        _txtLab = findViewById(R.id.txtLaboratorioPr);
        _txtDescripcion = findViewById(R.id.txtdescripcionPr);
        _txtNombreEstab = findViewById(R.id.txtEstablecimientoPr);
        _txtDireccion = findViewById(R.id.txtDireccionPr);
        _txtUbicacion = findViewById(R.id.txtUbicacionPr);

        global = new UserGlobal();
        pedido = new Pedido();
        producto = (Producto) getIntent().getSerializableExtra("productObjIntent");

        _txtPrecioUni.setText(formatoPrecio(producto.getPrecioUni()));
        _txtLab.setText(producto.getLaboratorio());
        _txtCategoria.setText(producto.getCategoria());
        _txtDescripcion.setText(producto.getDescripcion());
        _txtNombreEstab.setText(producto.getEstablecimiento());
        _txtDireccion.setText(producto.getDireccion());
        _txtUbicacion.setText(producto.getDistrito());

        CollapsingToolbarLayout ctl = findViewById(R.id.cToolbarL);
        ctl.setTitle(producto.getProducto());

        setSupportActionBar(_toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    //boton Agrega Producto
    public void agregaProducto(View view) {
        //Ventana Dialog Agrega Pedido
        LayoutInflater inflater = this.getLayoutInflater();
        final View v = inflater.inflate(R.layout.dialog_producto, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(v);
        final AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Pedido pedido = new Pedido();
        TextView _txtProducto = v.findViewById(R.id.txtProductoDialog);
        TextView _txtPrecio = v.findViewById(R.id.txtPrecioUniDialog);
        TextView _txtCantidad = v.findViewById(R.id.txtCantidadDialog);
        TextView _txtTotalPrecio = v.findViewById(R.id.txtPrecioTotalDialog);
        FloatingActionButton _btnAdd = v.findViewById(R.id.btnAdd);
        FloatingActionButton _btnSustract = v.findViewById(R.id.btnSubtract);
        Button _btnAgregaPedido = v.findViewById(R.id.btnAddPedidoDIalog);
        _txtProducto.setText(producto.getProducto());
        _txtPrecio.setText(formatoPrecio(producto.getPrecioUni()));
        _txtTotalPrecio.setText(formatoPrecio(producto.getPrecioUni()));
        cantidad = 1;
        //boton sumar cantidad
        _btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cantidad++;
                _txtCantidad.setText(String.valueOf(cantidad));
                _txtTotalPrecio.setText(formatoPrecio(producto.getPrecioUni() * cantidad));
            }
        });
        //boton restar cantidad
        _btnSustract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cantidad > 1) {
                    cantidad--;
                    _txtCantidad.setText(String.valueOf(cantidad));
                    _txtTotalPrecio.setText(formatoPrecio(producto.getPrecioUni() * cantidad));
                }
            }
        });
        //boton agregar productos
        _btnAgregaPedido.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (global.getListaPedidos() == null) {//si no hay productos
                    lista = new ArrayList<>();
                } else {//recuperamos si al menos hay un producto
                    lista = global.getListaPedidos();
                }

                pedido.setIdProducto(producto.getIdProducto());
                pedido.setProducto(producto.getProducto());
                pedido.setPrecioUni(producto.getPrecioUni());
                pedido.setEstablecimiento(producto.getEstablecimiento());
                //COMPRUEBA SI EL MISMO PRODUCTO EXISTE EN LA LISTA
                boolean addCantidad = false;
                for (int i = 0; i < lista.size(); i++) {
                    if (pedido.getIdProducto().equals(lista.get(i).getIdProducto())) {
                        pedido.setCantidad(lista.get(i).getCantidad() + cantidad);
                        lista.set(i, pedido);
                        addCantidad = true;
                    }
                }

                if (!addCantidad) {
                    pedido.setCantidad(cantidad);
                    lista.add(pedido);
                }

                Log.w("Datos------------->", String.valueOf(lista.size()));

                global.setListaPedidos(lista);
                Toast.makeText(getApplication(), "Se agregó productos a la  lista de pedidos", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                Intent intent = new Intent(getApplication(), MainActivity.class);
                startActivity(intent);
            }
        });
        dialog.show();
    }

    //boton MAPA
    public void btnMap(View v) {

        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("dato", producto);

        Dexter.withContext(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        startActivity(intent);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        Toast.makeText(getApplication(), "Active el permiso para ver su ubicación", Toast.LENGTH_SHORT).show();
                        startActivity(intent);
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {
                        startActivity(intent);
                    }

                }).check();

    }

    public String formatoPrecio(double precio) {
        DecimalFormat formato = new DecimalFormat("#0.00");
        return "S/. " + formato.format(precio);
    }

    //menu toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    //botones toolbar
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.itemPedidos:
                Intent intent = new Intent(this, PedidoActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}