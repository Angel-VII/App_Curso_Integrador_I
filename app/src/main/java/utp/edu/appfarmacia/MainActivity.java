package utp.edu.appfarmacia;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import utp.edu.appfarmacia.adaptadores.Adapta1;
import utp.edu.appfarmacia.clases.ConexionBase;
import utp.edu.appfarmacia.clases.Producto;
import utp.edu.appfarmacia.clases.UserGlobal;

public class MainActivity extends AppCompatActivity {

    EditText _edProductos;
    Spinner _spDistritos;
    RecyclerView _recyclerViewProduct;
    Toolbar _toolbar;
    List<Producto> productList;
    JsonObjectRequest jsonObjectRequest;
    JSONArray jsList;
    UserGlobal userGlobal;
    ConexionBase conexionBase;
    //String url = "http://10.0.2.2/farmaControlador/controlador.php";//url servidor local emulador
   // String url = "http://192.168.1.107/farmaControlador/controlador.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _edProductos = findViewById(R.id.edproductos);
        _spDistritos = findViewById(R.id.spDistritos);
        _toolbar = findViewById(R.id.toolbarBusqueda);
        _recyclerViewProduct = findViewById(R.id.rwListaProductos);

        _recyclerViewProduct.setVisibility(View.INVISIBLE);

        userGlobal = new UserGlobal();
        conexionBase = new ConexionBase();

        setSupportActionBar(_toolbar);
    }

    //búsqueda
    public void btnBuscar(View V) {
        _recyclerViewProduct.setVisibility(View.VISIBLE);
        String enlace = conexionBase.getUrl() + "?tag=consulta1&p=" + _edProductos.getText().toString() + "&d=" + _spDistritos.getSelectedItem().toString();
        productList = new ArrayList<>();
        //Log.w("PRUEBA 1",enlace);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, enlace, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    jsList = response.getJSONArray("dato");

                    for (int i = 0; i < jsList.length(); i++) {
                        JSONObject fila = (JSONObject) jsList.get(i);
                        Producto productObj = new Producto();
                        productObj.setIdProducto(fila.getString("idProduct"));
                        productObj.setProducto(fila.getString("nombre"));
                        productObj.setPrecioUni(fila.getDouble("precioUni"));
                        productObj.setStock(fila.getInt("stock"));
                        productObj.setCategoria(fila.getString("categoria"));
                        productObj.setLaboratorio(fila.getString("lab"));
                        productObj.setDescripcion(fila.getString("descrip"));
                        productObj.setEstablecimiento(fila.getString("nomEstab"));
                        productObj.setDistrito(fila.getString("distrito"));
                        productObj.setDireccion(fila.getString("direccion"));
                        productObj.setLatitud(fila.getDouble("latitud"));
                        productObj.setLongitud(fila.getDouble("longitud"));
                        productList.add(productObj);
                        Log.w("DATOS 2 : ---", productObj.getDireccion());
                    }

                    Log.w("DATOS: ---", productList.get(0).getLatitud() + "");
                    Adapta1 adaptador1 = new Adapta1(productList, getApplication());
                    _recyclerViewProduct.setLayoutManager(new LinearLayoutManager(getApplication()));
                    _recyclerViewProduct.setAdapter(adaptador1);

                } catch (Exception e) {
                    Toast.makeText(getApplication(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplication(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue cola = Volley.newRequestQueue(getApplication());
        cola.add(jsonObjectRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.itemPedidos:
                Intent intent = new Intent(this, PedidoActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}