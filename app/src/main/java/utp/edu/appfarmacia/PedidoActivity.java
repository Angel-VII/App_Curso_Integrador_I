package utp.edu.appfarmacia;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomappbar.BottomAppBar;

import org.json.JSONObject;

import java.text.DecimalFormat;

import utp.edu.appfarmacia.adaptadores.Adapta2;
import utp.edu.appfarmacia.clases.ConexionBase;
import utp.edu.appfarmacia.clases.ServicioEnvioEmail;
import utp.edu.appfarmacia.clases.UserGlobal;

public class PedidoActivity extends AppCompatActivity {

    TextView _txtPrecioTg;
    EditText _edDNI;
    EditText _edNombre;
    Toolbar _toolbar;
    BottomAppBar _bottomAppBarPedido;
    RadioGroup _rgTipoPago;
    RecyclerView _recyclerViewPedido;
    EditText _edCorreo;

    UserGlobal userGlobal;
    double precioTg = 0;
    JsonObjectRequest jobj;
    ConexionBase conexionBase;
    //String url = "http://192.168.1.107/farmaControlador/controlador.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos);
        //_txtUbicacion.setText(global.getLatitud() + " ");

        _edNombre = findViewById(R.id.edPersonName);
        _edDNI = findViewById(R.id.edDNI);
        _txtPrecioTg = findViewById(R.id.txtPrecioTotalG);
        _recyclerViewPedido = findViewById(R.id.rwListaPedidos);
        _toolbar = findViewById(R.id.toolbarPedidos);
        _rgTipoPago = findViewById(R.id.rgTipoPago);
        _edCorreo = findViewById(R.id.edCorreo);
        _bottomAppBarPedido = findViewById(R.id.bottomAppBarPedido);

        userGlobal = new UserGlobal();
        conexionBase = new ConexionBase(getApplication());

        setSupportActionBar(_toolbar);
      /* _bottomAppBarPedido.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
           @Override
           public boolean onMenuItemClick(MenuItem item) {
               return false;
           }
       });*/


        if (userGlobal.getListaPedidos() == null) {

            _recyclerViewPedido.setVisibility(View.INVISIBLE);
            Toast.makeText(this, "La lista esta vacía", Toast.LENGTH_SHORT).show();
        } else {
            for (int i = 0; i < userGlobal.getListaPedidos().size(); i++) {

                precioTg += userGlobal.getListaPedidos().get(i).getPrecioSubTotal();
            }
            _recyclerViewPedido.setVisibility(View.VISIBLE);
            Adapta2 adaptador = new Adapta2(userGlobal.getListaPedidos(), this);
            _recyclerViewPedido.setLayoutManager(new LinearLayoutManager(this));
            _recyclerViewPedido.setAdapter(adaptador);
        }

        _txtPrecioTg.setText("TOTAL: " + formatoPrecio(userGlobal.getPrecioTotal()));
    }

    //Enviar Pedidos
    public void EnviarPedidos(View v) {
        //COMPRUEBA LISTA
        if (userGlobal.getListaPedidos() != null) {
            //COMPRUEBA CAMPOS
            if (_edDNI.getText().toString().length() == 8 && _edNombre.getText().length() >= 2) {

                int tipoPago = _rgTipoPago.getCheckedRadioButtonId() == R.id.rbTarjeta ? 1 : 2;
                userGlobal.setTipoPago(tipoPago);
                userGlobal.setNombreUsuario(_edNombre.getText().toString());
                userGlobal.setDni(_edDNI.getText().toString());
                userGlobal.setCorreo(_edCorreo.getText().toString());

                //mensaje de confirmacion
                AlertDialog.Builder builder1 = new AlertDialog.Builder(v.getRootView().getContext());
                builder1.setTitle("Confirmación de Envío");
                builder1.setMessage("¿Desea enviar?");
                builder1.setNegativeButton("Cancelar", null);
                builder1.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //envio BD

                        envioListaPedido();

                        //envia correo
                        ServicioEnvioEmail.getInstance(getApplicationContext()).emailExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                ServicioEnvioEmail.getInstance(getApplicationContext()).SendEmail();
                            }
                        });
                        Toast.makeText(getApplication(), "Su lista de pedidos se envió con éxito", Toast.LENGTH_SHORT).show();
                    }
                });
                AlertDialog dialog = builder1.create();
                dialog.show();

            } else if (_edNombre.getText().length() < 2) {
                _edNombre.setError("Ingrese su nombre completo");

            } else if (_edDNI.getText().toString().length() < 8) {
                _edDNI.setError("Ingrese los 8 digitos de su DNI");
            }
            //Snackbar.make(v.getRootView(), "Complete todos los campos", Snackbar.LENGTH_LONG);
        } else Toast.makeText(this, "Lista vacía. Agregue un producto.", Toast.LENGTH_SHORT).show();

    }

    public String formatoPrecio(double precio) {
        DecimalFormat formato = new DecimalFormat("#0.00");
        return "S/. " + formato.format(precio);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.memu_toolbar_2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.itemPedidos:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    //PROCESO DE CONEXION Y ENVIO A BD
    public void envioListaPedido() {

        String enlace = conexionBase.getUrl() + "?tag=agregarpedido&dni=" + UserGlobal.getDni()
                + "&nombre=" + UserGlobal.getNombreUsuario()
                + "&latitud=" + UserGlobal.getLatitud()
                + "&longitud=" + UserGlobal.getLongitud()
                + "&tipopago=" + UserGlobal.getTipoPago();

        // CONEXION Principal RESPUESTA: idPedidos
        conexionBase.getIdPedidos(enlace, new ConexionBase.VolleyResponseListener() {
            @Override
            public void onResponse(String idPedidos) {

                RequestQueue cola2 = Volley.newRequestQueue(getApplication());
                //Toast.makeText(getApplication(), UserGlobal.getIdPedidos(), Toast.LENGTH_SHORT).show();
                for (int i = 0; i < UserGlobal.getListaPedidos().size(); i++) {

                    String enlace2 = conexionBase.getUrl() + "?tag=agregardetallepedido&idpedido=" + idPedidos
                            + "&idproducto=" + UserGlobal.getListaPedidos().get(i).getIdProducto()
                            + "&cantidad=" + UserGlobal.getListaPedidos().get(i).getCantidad();

                    jobj = new JsonObjectRequest(Request.Method.GET, enlace2, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                String msg = response.getString("msg");

                                Log.w("CONFIRMACION ->", msg);

                            } catch (Exception ex) {
                                Log.w("Error", ex.getMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.w("Error", error.getMessage());
                        }
                    });
                    jobj.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    cola2.add(jobj);

                } //Finalizacion total envio
            }

            @Override
            public void onError(String error) {
                Log.w("Error", error);
            }
        });

    }

}